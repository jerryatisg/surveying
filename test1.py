from scipy.optimize import fmin
from scipy.optimize import fsolve
import math

## script to show the lsa calcualtion to find the optimal coordinates of target point
## based on the mesaured distance from 4 base points 
## assume the 4 base points coordinates are know and accurate

## notice that target point is calculated on the coordinates of the base points 
## this could be an issue if base points are on moving object (such as vehicle) 

## 4 points and their distance to the target point (x, y, z, mesaured length to the target)
p=[[0,0,0,1.73],[0,0,1,1.41],[1,0,1,0.99],[1,0,0,1.41]]

## funtion representing the target value to minimize
lsfunc = lambda x: (((x[0]-p[0][0])**2 + (x[1]-p[0][1])**2 + (x[2]-p[0][2])**2 )**0.5 - p[0][3])**2 \
            +     (((x[0]-p[1][0])**2 + (x[1]-p[1][1])**2 + (x[2]-p[1][2])**2 )**0.5 - p[1][3])**2 \
            +     (((x[0]-p[2][0])**2 + (x[1]-p[2][1])**2 + (x[2]-p[2][2])**2 )**0.5 - p[2][3])**2 \
            +     (((x[0]-p[3][0])**2 + (x[1]-p[3][1])**2 + (x[2]-p[3][2])**2 )**0.5 - p[3][3])**2 

## pick 3 known points, find a solution for (x, y, z) as starting point for ls calculation
def equations(x):
    return (
            ((x[0]-p[0][0])**2 + (x[1]-p[0][1])**2 + (x[2]-p[0][2])**2)-p[0][3] ** 2, 
            ((x[0]-p[1][0])**2 + (x[1]-p[1][1])**2 + (x[2]-p[1][2])**2)-p[1][3] ** 2, 
            ((x[0]-p[2][0])**2 + (x[1]-p[2][1])**2 + (x[2]-p[2][2])**2)-p[2][3] ** 2
    )

## initial value as starting point to solve the equation
initval = [ 0.9,0.9,0.9 ]
## get solution of unknow point for those 3 known points and measured distance combination
r =  fsolve(equations, initval)

## use this solution as starting point for least square solution
initval=r

print "-------- ls result --------"
print fmin( lsfunc, initval )
print "---------------------------"

## comparing with the 4 different combinations of the the solution from any combination of 3 distances
## ie, if pick any 3 measured length from 3 base points, the target point can be calculated
## the coordinates of the target point from lsa calculation should fall within those calculated values
## ie, lsa should be the optimum values within the range defined by those calculated vlaues

r =  fsolve(equations, initval)
print '>1<'
print r 


def equations2(x):
    return (
            ((x[0]-p[0][0])**2 + (x[1]-p[0][1])**2 + (x[2]-p[0][2])**2)-p[0][3] ** 2, 
            ((x[0]-p[1][0])**2 + (x[1]-p[1][1])**2 + (x[2]-p[1][2])**2)-p[1][3] ** 2, 
            ((x[0]-p[3][0])**2 + (x[1]-p[3][1])**2 + (x[2]-p[3][2])**2)-p[3][3] ** 2
    )

r =  fsolve(equations2, initval)
print '>2<'
print r


def equations3(x):
    return (
            ((x[0]-p[1][0])**2 + (x[1]-p[1][1])**2 + (x[2]-p[1][2])**2)-p[1][3] ** 2, 
            ((x[0]-p[2][0])**2 + (x[1]-p[2][1])**2 + (x[2]-p[2][2])**2)-p[2][3] ** 2, 
            ((x[0]-p[3][0])**2 + (x[1]-p[3][1])**2 + (x[2]-p[3][2])**2)-p[3][3] ** 2
    )

r =  fsolve(equations3, initval)
print '>3<'
print r


def equations4(x):
    return (
            ((x[0]-p[0][0])**2 + (x[1]-p[0][1])**2 + (x[2]-p[0][2])**2)-p[0][3] ** 2, 
            ((x[0]-p[2][0])**2 + (x[1]-p[2][1])**2 + (x[2]-p[2][2])**2)-p[2][3] ** 2, 
            ((x[0]-p[3][0])**2 + (x[1]-p[3][1])**2 + (x[2]-p[3][2])**2)-p[3][3] ** 2
    )

r =  fsolve(equations4, initval)
print '>4<'
print r

## results should show the coordinates from least square solution should fall with the scopre of 4 solutions from the equation
